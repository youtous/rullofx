package rullofx.board.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by youtous on 28/09/2016.
 */
public class SampleBoardDataFactoryTest {
    private SampleBoardDataFactory sampleBoardDataFactory;
    @Before
    public void setUp() throws Exception {
        this.sampleBoardDataFactory = new SampleBoardDataFactory();
    }

    @Test
    public void createBoardData() throws Exception {
        int[] colSum = {1,10,15};
        int[] rowSum = {3,6,17};

        int[][] valuesCells = {
                {1,2,3},
                {4,5,6},
                {7,8,9}
        };

        BoardData boardData = this.sampleBoardDataFactory.createBoardData();

        // test col
        for (int i = 0; i < boardData.getColumnCount(); i++) {
            assertTrue("Col sum init",boardData.getColumnSum(i).getTarget() == colSum[i]);
        }

        // test row
        for (int i = 0; i < boardData.getColumnCount(); i++) {
            assertTrue("Row sum init",boardData.getRowSum(i).getTarget() == rowSum[i]);
        }

        //test each cells
        for (int row = 0; row < boardData.getRowCount() ; row++) {
            for (int col = 0; col < boardData.getRowCount(); col++) {
                assertEquals("Cells init",boardData.getCell(row, col), new Cell(valuesCells[row][col]));
            }
        }

    }

}