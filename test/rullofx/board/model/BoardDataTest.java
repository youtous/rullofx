package rullofx.board.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by youtous on 26/09/2016.
 */
public class BoardDataTest {

    BoardData data;

    @Before
    public void setUp() throws Exception {
        data = new BoardData(3, 3);
    }

    @Test
    public void testInitCell() {
        data.initCell(2,2,2);
        assertEquals("Cell init", data.getCell(2,2), new Cell(2));
    }

    @Test
    public void testInitColumnSum() {
        for (int row = 0; row < data.getRowCount(); row++) {
            for (int col = 0; col < data.getColumnCount(); col++) {
                data.initCell(row,col,1);
            }
        }
        data.initColumnSum(1,data.getRowCount());
        assertTrue("Somme test", data.getColumnSum(1).isTargetReached());
    }

    @Test
    public void testInitRowSum() {
        for (int row = 0; row < data.getRowCount(); row++) {
            for (int col = 0; col < data.getColumnCount(); col++) {
                data.initCell(row,col,1);
            }
        }
        data.initRowSum(1,data.getRowCount());
        assertTrue("Somme test", data.getRowSum(1).isTargetReached());
    }


}