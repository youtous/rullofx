package rullofx.board.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by youtous on 19/09/2016.
 */
public class CellTest {


    Cell cell;

    @org.junit.Before
    public void setUp() throws Exception {
        cell = new Cell(42);
    }

    /**
     * Ce test vérifie le comportement de la méthode toggleActiveState sur une
     * cellule active et dévérouillée.
     */
    @org.junit.Test
    public void testToggleActiveState_activeAndUnlockedCell() {
        boolean returnValue = cell.toggleActiveState();

        // la cellule n'étant pas verrouillée, l'opération doit avoir réussi et
        // la valeur de retour doit donc être vraie
        assertTrue("Valeur de retour incorrecte", returnValue);

        // la cellule étant initialement activée, elle doit maintenant être désactivée
        assertFalse("Etat de la cellule incorrect", cell.isActive());
    }


    /**
     * Ce test vérifie le comportement de la méthode toggleActiveState sur une
     * cellule inactive.
     */
    @org.junit.Test
    public void testToggleActiveState_inactiveCell() {

        // la cellule étant initialement activée, elle doit être désactivée
        assertTrue("Etat de la cellule incorrect", cell.isActive());

        boolean returnValue = cell.toggleActiveState();

        // la cellule n'étant pas verrouillée, l'opération doit avoir réussi et
        // la valeur de retour doit donc être vraie
        assertTrue("Valeur de retour incorrecte", returnValue);

        // cell state activated
        assertFalse("Etat de la cellule incorrect", cell.isActive());


    }

    /**
     * Ce test vérifie le comportement de la méthode testToggleLockState sur une
     * cellule active et dévérouillée.
     */
    @org.junit.Test
    public void testToggleLockState_activeAndUnlockedCell() {
        // default cell : activated !locked
        assertFalse("Cell mustn't be locked", cell.isLocked());
        assertTrue("Cell must be activated", cell.isActive());

        //starting test
        assertTrue("Cell toggle must be true", cell.toggleLockedState());


    }

    /**
     * Ce test vérifie le comportement de la méthode testToggleLockState sur une
     * cellule inactive.
     */
    @org.junit.Test
    public void testToggleLockState_inactiveCell() {

        cell.toggleActiveState();
        assertFalse("Cell must be desactivated", cell.isActive());

        //cell desactivated (by default) => toggle desactivated
        assertFalse("Cell toggle must be false when desactivated", cell.toggleLockedState());


    }


    /**
     * Ce test vérifie le comportement de la méthode testToggleLockState sur une
     * cellule active et vérouillée.
     */
    @org.junit.Test
    public void testToggleLockState_activeAndLockedCell() {
        // default cell : activated !locked
        cell.toggleLockedState();
        assertTrue("Cell must be locked", cell.isLocked());
        assertTrue("Cell must be activated", cell.isActive());

        //starting test
        assertTrue("Cell lock toggle must be true", cell.toggleLockedState());
    }


    /**
     * Ce test vérifie le comportement de la méthode toggleActiveState sur une
     * cellule inactive.
     */
    @org.junit.Test
    public void testToggleActiveState_activeAndLockedCell() {

        // default cell : activated !locked
        cell.toggleLockedState();
        assertTrue("Cell must be locked", cell.isLocked());
        assertTrue("Cell must be activated", cell.isActive());

        assertFalse("Cell activate toggle state must be false", cell.toggleActiveState());

    }


}