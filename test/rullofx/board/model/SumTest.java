package rullofx.board.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by youtous on 19/09/2016.
 */
public class SumTest {
    /**
     * Implémentation simple de l'interface Array dans une classe interne.
     */
    private class SimpleCellArray implements Array<Cell> {
        Cell [] data = {new Cell(1), new Cell(2), new Cell(3), new Cell(4)};

        @Override
        public int size() {
            return data.length;
        }

        @Override
        public Cell get(int index) {
            return data[index];
        }
    }

    private SimpleCellArray cells;
    private Sum sum;

    @Before
    public void setUp() throws Exception {
        this.cells = new SimpleCellArray();
        this.sum = new Sum(5, this.cells);
    }

    @Test
    public void testUpdate_noInactiveCell() {
        sum.update();

        // toutes les cellules étant actives, la somme doit valoir 10
        assertEquals(10, sum.getCurrent());
    }

    @Test
    public void testUpdate_noActiveCell() {
        for (int i = 0; i < cells.size(); i++) {
            Cell cell = cells.get(i);
            cell.toggleActiveState();
            assertFalse("Cells must be desactivated", cell.isActive());
        }

        sum.update();

        // toutes les cellules étant désactivées, la somme doit valoir 0
        assertEquals(0, sum.getCurrent());
    }

    @Test
    public void testUpdate_someActiveCell() {
        for (int i = 0; i < 2; i++) {
            Cell cell = cells.get(i);
            cell.toggleActiveState();
            assertFalse("Cells must be desactivated", cell.isActive());
        }

        sum.update();

        // cellules 0,1 désactivées 10 - 2 + 1 = 7 (somme attendue)
        assertEquals(7, sum.getCurrent());
    }

    @Test
    public void testTargetReached_reached() {
        assertTrue("Sum equals to 10", new Sum(10, this.cells).isTargetReached());

    }

    @Test
    public void testTargetReached_notReached() {
        assertFalse("Sum not equals to 8", new Sum(8, this.cells).isTargetReached());

    }

}