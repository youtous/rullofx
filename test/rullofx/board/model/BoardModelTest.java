package rullofx.board.model;

import org.junit.Before;
import org.junit.Test;

import java.util.EnumMap;
import java.util.Observable;
import java.util.Observer;

import static org.junit.Assert.*;

/**
 * Created by youtous on 28/09/2016.
 */
public class BoardModelTest implements Observer {
    BoardDataFactory factory = new SampleBoardDataFactory();
    BoardModel model;
    EnumMap<BoardModelEvent.EventType, BoardModelEvent> receivedEvents;

    @Before
    public void setUp() throws Exception {
        model = new BoardModel(factory);
        model.startGame();
        receivedEvents = new EnumMap<>(BoardModelEvent.EventType.class);
        model.addObserver(this);
    }


    @Test
    public void isSolved_solved() throws Exception {
        /*
        Solved :

         1   2 (3)
        (4) (5) 6
        (7)  8  9

        */
        this.model.boardData.getCell(0, 2).toggleActiveState();
        this.model.boardData.getCell(1, 1).toggleActiveState();
        this.model.boardData.getCell(1, 0).toggleActiveState();
        this.model.boardData.getCell(2, 0).toggleActiveState();

        assertTrue("Solved", this.model.isSolved());
    }

    @Test
    public void isSolved_notSolved() throws Exception {
        assertFalse("not solved", this.model.isSolved());
    }

    @Test
    public void reset() throws Exception {
        this.model.boardData.getCell(0, 2).toggleActiveState();
        this.model.boardData.getCell(1, 1).toggleActiveState();
        this.model.boardData.getCell(1, 0).toggleActiveState();
        this.model.boardData.getCell(2, 0).toggleActiveState();

        this.model.reset();

        for (int row = 0; row < this.model.getRowCount(); row++) {
            for (int col = 0; col < this.model.getColumnCount(); col++) {
                Cell cell = this.model.boardData.getCell(row, col);

                assertTrue("Default state", cell.isActive() && !cell.isLocked());
            }
        }

    }

    @Test
    public void toggleActiveState_unlockedCell() throws Exception {
        Cell cell = this.model.boardData.getCell(0, 0);
        assertTrue("unlocked cell", !cell.isLocked());

        boolean state = cell.isActive();
        this.model.toggleActiveState(0, 0);
        assertTrue("changed state cell", cell.isActive() != state);
        // Si l'état de la cellule est modifié,
        // les sommes de la ligne et de la colonne auxquelles appartient la cellule sont mises à jour.

        // ici sum row = 5, sum col = 11
        assertTrue("sum row updated", this.model.boardData.getRowSum(0).getCurrent() == 5);
        assertTrue("sum col updated", this.model.boardData.getColumnSum(0).getCurrent() == 12);

    }

    @Test
    public void toggleActiveState_lockedCell() throws Exception {
        Cell cell = this.model.boardData.getCell(0, 0);
        cell.toggleLockedState();
        assertTrue("locked cell", cell.isLocked());

        boolean state = cell.isActive();
        this.model.toggleActiveState(0, 0);
        assertTrue("changed state cell", cell.isActive() == state);
    }

    @Test
    public void toggleLockedState_activeCell() throws Exception {

        Cell cell = this.model.boardData.getCell(0, 0);
        assertTrue("activated cell", cell.isActive());

        boolean state = cell.isLocked();
        this.model.toggleLockedState(0, 0);
        assertTrue("changed state cell", cell.isLocked() != state);

    }

    @Test
    public void toggleLockedState_desactivatedCell() throws Exception {

        Cell cell = this.model.boardData.getCell(0, 0);
        cell.toggleActiveState();
        assertTrue("desactivated cell", !cell.isActive());

        boolean state = cell.isLocked();
        this.model.toggleLockedState(0, 0);
        assertTrue("changed state cell", cell.isLocked() == state);

    }

    @Test
    public void testStartGame_startEventFired() {
        // invocation d'une méthode censée émettre un événement
        model.startGame();
        // récupération de l'événement attendu
        BoardModelEvent event = receivedEvents.get(BoardModelEvent.EventType.START_EVENT);
        // vérification de sa présence
        assertNotNull(event);
    }

    @Override
    public void update(Observable o, Object arg) {
        BoardModelEvent event = (BoardModelEvent) arg;
        receivedEvents.put(event.eventType, event);
    }

    @Test
    public void test_cellActivatingStateEventFired() {
        Cell cell = model.boardData.getCell(0, 0);
        assertFalse("cell mustn't be locked", cell.isLocked());
        assertTrue("cell must be activated", cell.isActive());

        model.toggleActiveState(0, 0);
        assertFalse("cell deactivated", cell.isActive());

        BoardModelEvent event = receivedEvents.get(BoardModelEvent.EventType.ACTIVATION_EVENT);
        assertNotNull(event);

        assertTrue("Row & Col", event.column == 0 && event.row == 0);
    }

    @Test
    public void test_cellActivatingStateEventNotFired() {
        Cell cell = model.boardData.getCell(0, 0);
        cell.toggleLockedState();
        assertTrue("cell must be locked", cell.isLocked());

        model.toggleActiveState(0, 0);

        BoardModelEvent event = receivedEvents.get(BoardModelEvent.EventType.ACTIVATION_EVENT);
        assertNull(event);
    }

    @Test
    public void test_cellLockingStateEventFired() {
        Cell cell = model.boardData.getCell(0, 0);
        assertFalse("cell mustn't be locked", cell.isLocked());
        assertTrue("cell must be activated", cell.isActive());

        model.toggleLockedState(0, 0);
        assertTrue("cell locked", cell.isLocked());

        BoardModelEvent event = receivedEvents.get(BoardModelEvent.EventType.LOCK_EVENT);
        assertNotNull(event);

        assertTrue("Row & Col", event.column == 0 && event.row == 0);

    }

    @Test
    public void test_cellLockingStateEventNotFired() {
        Cell cell = model.boardData.getCell(0, 0);
        assertFalse("cell mustn't be locked", cell.isLocked());
        cell.toggleActiveState();
        assertFalse("cell must not be activated", cell.isActive());

        model.toggleLockedState(0, 0);

        BoardModelEvent event = receivedEvents.get(BoardModelEvent.EventType.LOCK_EVENT);
        assertNull(event);
    }

    @Test
    public void test_ColSumReachedEventFired() {
        //solve 1rst col
        this.model.toggleActiveState(1, 0);
        this.model.toggleActiveState(2, 0);


        BoardModelEvent event = receivedEvents.get(BoardModelEvent.EventType.REACHED_COLUMN_EVENT);
        assertNotNull(event);

        assertTrue("Col", event.column == 0);
    }

    @Test
    public void test_ColSumReachedEventNotFired() {

        Cell cell = model.boardData.getCell(1, 0);
        cell.toggleLockedState();
        assertTrue("cell must be locked", cell.isLocked());


        //not solved 1rst col
        this.model.toggleActiveState(1, 0);


        BoardModelEvent event = receivedEvents.get(BoardModelEvent.EventType.REACHED_COLUMN_EVENT);
        assertNull(event);
    }

    @Test
    public void test_RowSumReachedEventFired() {
        //solve 1rst row
        this.model.toggleActiveState(0, 2);


        BoardModelEvent event = receivedEvents.get(BoardModelEvent.EventType.REACHED_ROW_EVENT);
        assertNotNull(event);

        assertTrue("Row", event.row == 0);
    }

    @Test
    public void test_RowSumReachedEventNotFired() {

        Cell cell = model.boardData.getCell(0, 2);
        cell.toggleLockedState();
        assertTrue("cell must be locked", cell.isLocked());


        //not solved 2nd row
        this.model.boardData.getCell(0, 2).toggleActiveState();


        BoardModelEvent event = receivedEvents.get(BoardModelEvent.EventType.REACHED_COLUMN_EVENT);
        assertNull(event);
    }

    @Test
    public void test_GameSolvedEventFired() {

        this.model.toggleActiveState(0,2);
        this.model.toggleActiveState(1,1);
        this.model.toggleActiveState(1,0);
        this.model.toggleActiveState(2,0);

        assertTrue("Solved", this.model.isSolved());


        BoardModelEvent event = receivedEvents.get(BoardModelEvent.EventType.SOLVED_EVENT);
        assertNotNull(event);
    }

    @Test
    public void test_GameSolvedEventNotFired() {

        Cell cell = model.boardData.getCell(0, 2);
        cell.toggleLockedState();
        assertTrue("cell must be locked", cell.isLocked());


        //solve the game
        this.model.toggleActiveState(0,2);
        this.model.toggleActiveState(1,1);
        this.model.toggleActiveState(1,0);
        this.model.toggleActiveState(2,0);


        BoardModelEvent event = receivedEvents.get(BoardModelEvent.EventType.SOLVED_EVENT);
        assertNull(event);
    }
}