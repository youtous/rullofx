package rullofx;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import rullofx.board.BoardView;
import rullofx.board.model.BoardModel;
import rullofx.board.model.BoardModelEvent;

import java.util.Observable;
import java.util.Observer;

public class RulloController {
    @FXML
    StackPane stackPane;
    private BoardView boardView;

    @FXML
    Label winLabel;

    /**
     * Méthode invoquée automatiquement après la création de l'interface graphique * associée à ce contrôleur.
     */
    public void initialize() {
        stackPane.getChildren().add(boardView = new BoardView());
        BoardModel boardModel = new BoardModel();
        this.boardView.setModel(boardModel);
        this.winLabel = new Label("✓");
        this.winLabel.getStyleClass().add("win");
        boardModel.startGame();
        boardModel.addObserver(
                (o, arg) -> {
                    switch (((BoardModelEvent) arg).eventType) {
                        case SOLVED_EVENT:
                            RulloController.this.stackPane.getChildren().add(RulloController.this.winLabel);
                            RulloController.this.boardView.setOpacity(0.2);
                            RulloController.this.stackPane.setMouseTransparent(true);
                            RulloController.this.winLabel.toFront();
                            break;
                    }
                }
        );
    }

    public void onNewGame() {
        this.stackPane.getChildren().removeAll(this.stackPane.getChildren());
        this.stackPane.setMouseTransparent(false);
        this.initialize();
    }

    public void onReset() {
        this.boardView.getModel().reset();
    }
}
