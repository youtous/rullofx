/**
 *
 */
package rullofx.board;


import javafx.scene.input.MouseEvent;


public class BoardController {
    private BoardView boardView;

    public BoardController(BoardView boardView) {
        this.boardView = boardView;
    }
    /**
     * Change l'état de la cellule :
     * bouton primaire : toggleActivationState
     * bouton secondaire : toggleLockedState
     *
     * @param event
     * @param row
     * @param col
     */
    public void onCellClicked(MouseEvent event, int row, int col) {
        //System.out.println("[" + event.getX() + "," + event.getY() + "]" + "row : " + row + ", " + "col : " + col);

        switch (event.getButton()) {
            case PRIMARY:
                this.boardView.model.toggleActiveState(row, col);
                break;
            case SECONDARY:
                this.boardView.model.toggleLockedState(row, col);
                break;
        }
    }

    /**
     * Méthode invoquée en réponse à un clic sur une somme de colonne.
     *
     * @param column - colonne de la somme cliquée
     */
    public void onColumnSumClicked(int column) {
        if (this.boardView.model.isColumnTargetReached(column)) {
            for (int row = 0; row < this.boardView.model.getRowCount(); row++) {
                if(!this.boardView.model.isCellLocked(row, column)) {
                this.boardView.model.toggleLockedState(row, column);
                }
            }
        }
    }

    /**
     * Méthode invoquée en réponse à un clic sur une somme de ligne.
     *
     * @param row - ligne de la somme cliquée
     */
    public void onRowSumClicked(int row) {
        if (this.boardView.model.isRowTargetReached(row)) {
            for (int column = 0; column < this.boardView.model.getColumnCount(); column++) {
                if(!this.boardView.model.isCellLocked(row, column)) {
                    this.boardView.model.toggleLockedState(row, column);
                }
            }
        }
    }
}
