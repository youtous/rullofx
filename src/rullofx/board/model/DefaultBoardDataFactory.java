package rullofx.board.model;

import java.util.Random;

/**
 * Created by youtous on 08/11/2016.
 * <p>
 * Générateur de grilles aléatoires.
 * <p>
 * Les grilles générées ont les caractéristiques suivantes :
 * <p>
 * Nombre de lignes et de colonnes compris entre 5 et 8
 * Valeurs des cellules comprises déterminées aléatoirement dans un des intervalles suivants (également déterminé aléatoirement) :
 * [1-9]
 * [2-4]
 * [1-19]
 * La résolution de la grille nécessite de désactiver en moyenne 40% des cellules.
 */
public class DefaultBoardDataFactory implements BoardDataFactory {
    private static Random rand = new Random();

    public enum Range {
        RANGE_1_9(1, 9),
        RANGE_2_4(2, 4),
        RANGE_1_19(1, 19);

        public final int min;
        public final int max;

        Range(int min, int max) {
            this.min = min;
            this.max = max;
        }
    }

    @Override
    public BoardData createBoardData() {
        int rows = DefaultBoardDataFactory.rand.nextInt(8 - 5) + 5;
        int cols = DefaultBoardDataFactory.rand.nextInt(8 - 5) + 5;

        BoardData boardData = new BoardData(rows, cols);

        Range range = Range.values()[DefaultBoardDataFactory.rand.nextInt(3)];
        int colsValues[] = new int[cols];
        int rowsValues[] = new int[rows];


        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                int cellValue = DefaultBoardDataFactory.rand.nextInt(range.max - range.min) + range.min + 1;
                boardData.initCell(row, col,
                        cellValue
                );

                int rand = DefaultBoardDataFactory.rand.nextInt(100);
                if (rand > 40){
                    colsValues[col] += cellValue;
                    rowsValues[row] += cellValue;
                }
            }
        }

        for (int row = 0; row < rows; row++) {
            boardData.initRowSum(row, rowsValues[row]);
        }

        for (int col = 0; col < cols; col++) {
            boardData.initColumnSum(col, colsValues[col]);
        }

        return boardData;
    }
}
