package rullofx.board.model;

/**
 * Created by youtous on 19/09/2016.
 */
public class Cell {
    private int value;
    private boolean active = true;
    private boolean locked;

    public Cell(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isLocked() {
        return locked;
    }

    /**
     * Inverse l'état d'activation de la cellule. Si la cellule est verrouillée, l'état ne sera pas modifié.
     * @return vrai si l'état a été modifié, faux sinon
     */
    public boolean toggleActiveState() {
        if(this.locked) {
            return false;
        }

        this.active = !this.active;
        return true;
    }

    /**
     * Inverse l'état de verrouillage de la cellule. Si la cellule est désactivée, l'état ne sera pas modifié.
     * @return vrai si l'état a été modifié, faux sinon
     */
    public boolean toggleLockedState() {
        if(this.active) {
            this.locked = !this.locked;
            return true;
        }

        return false;
    }

    @Override
    public boolean equals(Object obj) {
        boolean same = super.equals(obj);
        if(obj == this) {
            return true;
        }
        else if (obj instanceof Cell) {
            Cell compared = (Cell) obj;
            return compared.active == this.active && compared.locked == this.locked && compared.value == this.value;
        }
        return false;
    }
}
