package rullofx.board.model;

/**
 * Created by youtous on 28/09/2016.
 */
public class SampleBoardDataFactory implements BoardDataFactory{

    @Override
    public BoardData createBoardData() {
        BoardData boardData = new BoardData(3,3);

        int init = 1;
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3 ; col++) {
                boardData.initCell(row,col,init++);
            }
        }

        boardData.initRowSum(0,3);
        boardData.initRowSum(1,6);
        boardData.initRowSum(2,17);

        boardData.initColumnSum(0,1);
        boardData.initColumnSum(1,10);
        boardData.initColumnSum(2,15);

        return boardData;
    }
}
