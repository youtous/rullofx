package rullofx.board.model;

/**
 * Created by youtous on 30/09/2016.
 */
public class BoardModelEvent {
    public EventType eventType;
    public final int row;
    public final int column;

    public enum EventType {
        ACTIVATION_EVENT,
        LOCK_EVENT,
        REACHED_COLUMN_EVENT,
        REACHED_ROW_EVENT,
        START_EVENT,
        SOLVED_EVENT,
    }


    public BoardModelEvent(EventType eventType) {
        this.row = 0;
        this.column = 0;
        this.eventType = eventType;
    }

    public BoardModelEvent(EventType eventType, int row, int column) {
        this.row = row;
        this.column = column;
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "[" + this.row + "][" + this.column + "] " + this.eventType;
    }
}
