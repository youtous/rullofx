/**
 *
 */
package rullofx.board.model;

import java.util.Observable;

public class BoardModel extends Observable {
    protected BoardData boardData;
    private BoardDataFactory boardDataFactory;

    public BoardModel() {
        this.boardData = null;
        this.boardDataFactory = new DefaultBoardDataFactory();
    }

    public BoardModel(BoardDataFactory boardDataFactory) {
        this.boardData = null;
        this.boardDataFactory = boardDataFactory;
    }


    public int getColumnCount() {
        return this.boardData.getColumnCount();
    }

    public int getColumnTarget(int col) {
        return this.boardData.getColumnSum(col).getTarget();
    }

    public int getRowCount() {
        return this.boardData.getRowCount();

    }

    public int getRowTarget(int row) {
        return this.boardData.getRowSum(row).getTarget();

    }

    public int getValue(int row, int col) {
        return this.boardData.getCell(row, col).getValue();

    }

    public boolean isCellActive(int row, int col) {
        return this.boardData.getCell(row, col).isActive();
    }

    public boolean isCellLocked(int row, int col) {
        return this.boardData.getCell(row, col).isLocked();

    }

    public boolean isColumnTargetReached(int col) {
        this.boardData.getColumnSum(col).update();
        return this.boardData.getColumnSum(col).isTargetReached();

    }

    public boolean isRowTargetReached(int row) {
        this.boardData.getRowSum(row).update();
        return this.boardData.getRowSum(row).isTargetReached();

    }

    public boolean isSolved() {
        boolean solved = true;

        //check rows
        int row = 0;
        while (solved && row < this.boardData.getRowCount()) {
            solved = this.isRowTargetReached(row);
            row += 1;
        }
        int col = 0;
        while (solved && col < this.boardData.getColumnCount()) {
            solved = this.isColumnTargetReached(col);
            col += 1;
        }

        return solved;
    }

    /**
     * Démarre une nouvelle partie.
     */
    public void startGame() {
        boardData = boardDataFactory.createBoardData();

        // marque le modèle comme ayant changé
        this.setChanged();

        // émission d'un événement à destination des observateurs du modèle
        this.notifyObservers(new BoardModelEvent(BoardModelEvent.EventType.START_EVENT));
    }

    /**
     * Réinitialise le plateau (sans changer les données). Les cellules sont toutes activées et dévérouillées.
     */
    public void reset() {
        for (int row = 0; row < this.getRowCount(); row++) {
            for (int col = 0; col < this.getColumnCount(); col++) {
                Cell cell = this.boardData.getCell(row, col);

                if (cell.isLocked()) {
                    this.toggleLockedState(row, col);
                }

                if (!cell.isActive()) {
                    this.toggleActiveState(row, col);
                }


            }
        }
    }

    /**
     * Inverse l'état d'activation d'une cellule. Si la cellule est verrouillée, l'état ne sera pas modifié.
     * Si l'état de la cellule est modifié, les sommes de la ligne et de la colonne auxquelles appartient la
     * cellule sont mises à jour.
     *
     * @param row ligne de la cellule
     * @param col colonne de la cellule
     */
    public void toggleActiveState(int row, int col) {
        Cell cell = this.boardData.getCell(row, col);
        if (!cell.isLocked()) {
            cell.toggleActiveState();
            this.setChanged();
            this.notifyObservers(new BoardModelEvent(BoardModelEvent.EventType.REACHED_ROW_EVENT, row, col));


            this.setChanged();
            this.notifyObservers(new BoardModelEvent(BoardModelEvent.EventType.REACHED_COLUMN_EVENT, row, col));


            if (this.isSolved()) {
                this.setChanged();
                this.notifyObservers(new BoardModelEvent(BoardModelEvent.EventType.SOLVED_EVENT, row, col));
            }

            this.setChanged();
            this.notifyObservers(new BoardModelEvent(BoardModelEvent.EventType.ACTIVATION_EVENT, row, col));
        }
    }

    /**
     * Inverse l'état de verrouillage d'une cellule. Si la cellule est désactivée, l'état ne sera pas modifié.
     *
     * @param row ligne de la cellule
     * @param col colonne de la cellule
     */
    public void toggleLockedState(int row, int col) {
        Cell cell = this.boardData.getCell(row, col);
        if (cell.toggleLockedState()) {
            this.setChanged();
            this.notifyObservers(new BoardModelEvent(BoardModelEvent.EventType.LOCK_EVENT, row, col));
        }
    }

    @Override
    public String toString() {
        String result = "   ";
        for (int col = 0; col < this.getColumnCount(); col++) {

            result += "|" + this.boardData.getColumnSum(col).getTarget() + "|  ";
        }

        result += "\n";

        for (int row = 0; row < this.getRowCount(); row++) {
            result += "|" + this.boardData.getRowSum(row).getTarget() + "|";
            for (int col = 0; col < this.getColumnCount(); col++) {
                Cell cell = this.boardData.getCell(row, col);

                if (cell.isActive()) {
                    result += " " + cell.getValue() + " ";
                } else {
                    result += "(" + cell.getValue() + ")";
                }
                result += " ";
            }
            result += "\n";
        }
        return result;
    }
}
