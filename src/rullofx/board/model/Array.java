package rullofx.board.model;

/**
 * Created by youtous on 19/09/2016.
 * Interface générique représentant une structure de données indexée (comme un tableau) en lecture seule.
 */
public interface Array<T> {

    /**
     * Retourne le nombre d'éléments du tableau
     *
     * @return nombre éléments
     */
    int size();

    /**
     * Retourne un élément du tableau
     *
     * @return élément
     * @params index - indice de l'élément à retourner
     */
    T get(int index);
}
