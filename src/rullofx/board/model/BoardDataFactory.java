package rullofx.board.model;

/**
 * Created by youtous on 26/09/2016.
 */
public interface BoardDataFactory {

    BoardData createBoardData();
}
