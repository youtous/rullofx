package rullofx.board.model;

/**
 * Created by youtous on 19/09/2016.
 * <p>
 * Représente une somme de cellules. Cette classe gère à la fois la somme à
 * atteindre et la somme courante, calculée en fonction de l'état des cellules à prendre à compte.
 * <p>
 * Important :
 * en l'absence d'un moyen de détection du changement d'état d'une cellule, une somme ne se met
 * pas à jour automatiquement : il est nécessaire d'invoquer la méthode update après toute activation
 * ou désactivation d'une cellule dont dépend la somme.
 */
public class Sum {

    private int target;
    private int current;
    private Array<Cell> cells;


    /**
     * Constructeur
     *
     * @params target - valeur à atteindre.
     * cells - cellules à prendre en compte pour le calcul de cette somme
     */
    public Sum(int target, Array<Cell> cells) {
        this.target = target;
        this.cells = cells;
        this.update();
    }

    /**
     * Retourne la valeur courante de la somme (calculée lors du dernier appel à update
     *
     * @return valeur courante
     */
    public int getCurrent() {
        return this.current;
    }

    /**
     * Retourne la valeur à atteindre
     *
     * @return valeur à atteindre
     */
    public int getTarget() {
        return this.target;
    }

    /**
     * Indique si la valeur courante est égale à la valeur à atteindre
     *
     * @return vrai si la valeur courante est égale à la valeur à atteindre, faux sinon
     */
    public boolean isTargetReached() {
        return this.target == this.current;
    }

    /**
     * Met à jour la somme courante, en ne tenant compte que des cellules actives.
     */
    public void update() {
        this.current = 0;
        for (int i = 0; i < this.cells.size(); i++) {
            Cell cell = this.cells.get(i);
            if (cell.isActive()) {
                this.current += cell.getValue();
            }
        }
    }

}
