package rullofx.board.model;

/**
 * Created by youtous on 26/09/2016.
 * *
 * Données du plateau de jeu
 */
public class BoardData {
    private int columnCount;
    private int rowCount;
    private Cell[][] cells;
    private Sum[] columnSums;
    private Sum[] rowSums;

    /**
     * Constructeur
     *
     * @param rowCount    nombre de lignes
     * @param columnCount nombre de colonnes
     */
    public BoardData(int rowCount, int columnCount) {
        this.rowCount = rowCount;
        this.columnCount = columnCount;
        this.columnSums = new Sum[columnCount];
        this.rowSums = new Sum[rowCount];
        this.cells = new Cell[rowCount][columnCount];
    }

    /**
     * Retourne le nombre de colonnes du plateau de jeu
     *
     * @return nombre de colonnes
     */
    public int getColumnCount() {
        return columnCount;
    }

    /**
     * Retourne le nombre de lignes du plateau de jeu
     *
     * @return nombre de lignes
     */
    public int getRowCount() {
        return rowCount;
    }

    /**
     * Retourne une cellule du plateau de jeu
     *
     * @param row    ligne de la cellule
     * @param column colonne de la cellule
     * @return cellule
     */
    public Cell getCell(int row, int column) {
        return cells[row][column];
    }

    /**
     * Initialise une cellule du plateau de jeu.
     *
     * @param row    ligne de la cellule
     * @param column colonne de la cellule
     * @param value  valeur de la cellule
     */
    public void initCell(int row, int column, int value) {
        this.cells[row][column] = new Cell(value);
    }

    /**
     * Retourne la somme d'une colonne du plateau de jeu
     *
     * @param column colonne de la somme
     * @return somme
     */
    public Sum getColumnSum(int column) {
        return columnSums[column];
    }

    /**
     * Initialise une somme de colonne du plateau de jeu
     *
     * @param column colonne de la somme
     * @param target somme à atteindre pour la colonne
     */
    public void initColumnSum(int column, int target) {
        this.columnSums[column] = new Sum(target, new Column(column));
    }

    /**
     * Retourne la somme d'une ligne du plateau de jeu
     *
     * @param row ligne de la somme
     * @return somme
     */
    public Sum getRowSum(int row) {
        return rowSums[row];
    }

    /**
     * Initialise une somme de ligne du plateau de jeu
     *
     * @param row    ligne de la somme
     * @param target somme à atteindre pour la ligne
     */
    public void initRowSum(int row, int target) {
        this.rowSums[row] = new Sum(target, new Row(row));
    }

    public class Row implements Array<Cell> {
        // classe interne car on accède aux attributs privates de la classe board data
        private int row;

        public Row(int row) {
            this.row = row;
        }

        @Override
        public int size() {
            return BoardData.this.cells[0].length;
        }

        @Override
        public Cell get(int col) {
            return BoardData.this.cells[this.row][col];
        }
    }

    public class Column implements Array<Cell> {
        // classe interne car on accède aux attributs privates de la classe board data
        private int column;

        public Column(int column) {
            this.column = column;
        }

        @Override
        public int size() {
            return BoardData.this.cells.length;
        }

        @Override
        public Cell get(int row) {
            return BoardData.this.cells[row][this.column];
        }
    }
}
