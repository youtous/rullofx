/**
 *
 */
package rullofx.board;

import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import rullofx.board.model.BoardModel;
import rullofx.board.model.BoardModelEvent;

import java.util.Observable;
import java.util.Observer;

public class BoardView extends GridPane implements Observer {
    private BoardController controller = new BoardController(this);
    protected BoardModel model;

    private CellView[][] cellViews;

    private RowSumView[] leftSumViews;
    private RowSumView[] rightSumViews;

    private ColSumView[] topSumViews;
    private ColSumView[] bottomSumViews;


    public void init() {
        this.cellViews = new CellView[this.model.getRowCount()][this.model.getColumnCount()];

        this.leftSumViews = new RowSumView[this.model.getRowCount()];
        this.rightSumViews = new RowSumView[this.model.getRowCount()];

        this.topSumViews = new ColSumView[this.model.getColumnCount()];
        this.bottomSumViews = new ColSumView[this.model.getColumnCount()];


        for (int rowPane = 0; rowPane < this.model.getRowCount() + 2; rowPane++) {
            int rowGame = rowPane - 1;

            if (rowPane != 0 && rowPane != (this.model.getRowCount() + 1)) {
                this.leftSumViews[rowGame] = new RowSumView(rowGame);
                this.rightSumViews[rowGame] = new RowSumView(rowGame);
            }

            for (int colPane = 0; colPane < this.model.getColumnCount() + 2; colPane++) {
                int colGame = colPane - 1;

                if (rowPane != 0 && rowPane != (this.model.getRowCount() + 1)) {
                    if (colPane == 0) {
                        this.add(this.leftSumViews[rowGame], colPane, rowPane);
                    } else if (colPane == this.model.getColumnCount() + 1) {
                        this.add(this.rightSumViews[rowGame], colPane, rowPane);
                    } else {
                        this.cellViews[rowGame][colGame] = new CellView(rowGame, colGame);
                        this.add(this.cellViews[rowGame][colGame], colPane, rowPane);
                    }
                } else if (rowPane != 0 && rowPane != (this.model.getRowCount() + 1)) {
                    this.topSumViews[colGame] = new ColSumView(colGame);
                    this.bottomSumViews[colGame] = new ColSumView(colGame);
                } else if (colPane != 0 && colPane != (this.model.getColumnCount() + 1)) {
                    if (rowPane == 0) {
                        this.topSumViews[colGame] = new ColSumView(colGame);
                        this.add(this.topSumViews[colGame], colPane, rowPane);
                    } else if (rowPane == (this.model.getRowCount() + 1)) {
                        this.bottomSumViews[colGame] = new ColSumView(colGame);
                        this.add(this.bottomSumViews[colGame], colPane, rowPane);
                    }
                }
            }
        }
    }

    public BoardModel getModel() {
        return model;
    }

    public void setModel(BoardModel model) {

        if (this.model != null) {
            this.model.deleteObserver(this);
        }
        this.model = model;
        this.model.addObserver(this);
    }

    /**
     * This method is called whenever the observed object is changed. An
     * application calls an <tt>Observable</tt> object's
     * <code>notifyObservers</code> method to have all the object's
     * observers notified of the change.
     *
     * @param o   the observable object.
     * @param arg an argument passed to the <code>notifyObservers</code>
     */
    @Override
    public void update(Observable o, Object arg) {
        System.out.println(arg);
        switch (((BoardModelEvent) arg).eventType) {
            case START_EVENT:
                this.init();
                break;
            case ACTIVATION_EVENT:
                this.cellViews[((BoardModelEvent) arg).row][((BoardModelEvent) arg).column].updateActiveState();
                break;
            case LOCK_EVENT:
                this.cellViews[((BoardModelEvent) arg).row][((BoardModelEvent) arg).column].updateLockedState();
                break;
            case REACHED_ROW_EVENT:
                this.leftSumViews[((BoardModelEvent) arg).row].update();
                this.rightSumViews[((BoardModelEvent) arg).row].update();
                break;
            case REACHED_COLUMN_EVENT:
                this.topSumViews[((BoardModelEvent) arg).column].update();
                this.bottomSumViews[((BoardModelEvent) arg).column].update();
                break;
        }
    }

    public class CellView extends Label {
        private int row;
        private int col;

        public CellView(int row, int col) {
            this.col = col;
            this.row = row;
            this.setText("" + BoardView.this.model.getValue(this.row, this.col));

            this.getStyleClass().add(
                    "cell"
            );
            this.getStyleClass().add(
                    "active"
            );

            this.setOnMouseClicked(
                    new EventHandler<MouseEvent>() {
                        /**
                         * Invoked when a specific event of the type for which this handler is
                         * registered happens.
                         *
                         * @param event the event which occurred
                         */
                        @Override
                        public void handle(MouseEvent event) {
                            BoardView.this.controller.onCellClicked(event, CellView.this.row, CellView.this.col);
                        }
                    });
        }

        /**
         * Met à jour le style de la cellule d'après son état : active ou inactive
         */
        public void updateActiveState() {

            if (BoardView.this.model.isCellActive(row, col)) {
                this.getStyleClass().add("active");
                this.getStyleClass().remove("inactive");
            } else {
                this.getStyleClass().add("inactive");

            }

        }

        /**
         * Met à jour le style de la cellule locked
         */
        public void updateLockedState() {
            if (BoardView.this.model.isCellLocked(row, col)) {
                this.getStyleClass().add("locked");
            } else {
                this.getStyleClass().remove("locked");

            }
        }
    }

    public class SumView extends Label {

        /**
         * @param target - valeur à atteindre
         */
        public SumView(int target) {
            super("" + target);
            this.getStyleClass().add("sum");
        }

        /**
         * Met à jour le style de la vue en fonction de la valeur du paramètre.
         */
        public void setReached(boolean reached) {
            if (reached) {
                this.getStyleClass().add("correct");
            } else {
                this.getStyleClass().remove("correct");
            }

        }
    }

    public class RowSumView extends SumView {
        private int row;

        /**
         * @param row - indice de la ligne de la somme représentée par cette vue.
         */
        public RowSumView(int row) {
            super(BoardView.this.model.getRowTarget(row));
            this.row = row;
            this.update();
            this.setOnMouseClicked(
                    new EventHandler<MouseEvent>() {
                        /**
                         * Invoked when a specific event of the type for which this handler is
                         * registered happens.
                         *
                         * @param event the event which occurred
                         */
                        @Override
                        public void handle(MouseEvent event) {
                            BoardView.this.controller.onRowSumClicked(RowSumView.this.row);
                        }
                    });
        }

        /**
         * Met à jour le style de la vue ("reached" ou non, en fonction de l'état du modèle).
         */
        public void update() {
            super.setReached(BoardView.this.model.isRowTargetReached(row));
        }
    }

    public class ColSumView extends SumView {
        private int col;

        /**
         * @param col - indice de la colonne de la somme représentée par cette vue.
         */
        public ColSumView(int col) {
            super(BoardView.this.model.getColumnTarget(col));
            this.col = col;
            this.update();
            this.setOnMouseClicked(
                    new EventHandler<MouseEvent>() {
                        /**
                         * Invoked when a specific event of the type for which this handler is
                         * registered happens.
                         *
                         * @param event the event which occurred
                         */
                        @Override
                        public void handle(MouseEvent event) {
                            BoardView.this.controller.onColumnSumClicked(ColSumView.this.col);
                        }
                    });

        }

        /**
         * Met à jour le style de la vue ("reached" ou non, en fonction de l'état du modèle).
         */
        public void update() {
            super.setReached(BoardView.this.model.isColumnTargetReached(col));
        }
    }
}